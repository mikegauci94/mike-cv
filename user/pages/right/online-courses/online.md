---
title: ONLINE COURSES
date: 13:34 06/21/2015 

courses:
    - title: Back to School Web Development and Programming Bundle
      link_name: John Bura
      link: https://www.udemy.com/back-to-school-web-development-and-programming-bundle/
      animation: fadeIn
      year: 2014
    - title: Learn To Build Beautiful HTML5 And CSS3 Websites In 1 Month
      link_name: Ryan Bonhardt
      link: https://www.udemy.com/learn-to-build-beautiful-html5-and-css3-websites-in-1-month/
      animation: fadeIn
      year: 2015
    - title: Projects in HTML5
      link_name: Eduonix Learning Solutions
      link: https://www.udemy.com/projects-in-html5/
      animation: fadeIn
      year: 2015
    - title: JavaScript Understanding the Weird Parts
      link_name: Anthony Alicea
      link: https://www.udemy.com/understand-javascript/
      animation: fadeIn
      year: 2015
    - title: '"Joomla 3: Develop a Professional Website in 3 Simple Steps"'
      link_name: Angelos Giorgis
      link: https://www.udemy.com/develop-a-professional-hotel-website-using-joomla-3x/?src=sac&kw=joomla%203
      animation: fadeIn
      year: 2015  
    - title: jQuery Fundamentals Training
      link_name: Webucator Training
      link: https://www.udemy.com/jquery-fundamentals-training/?src=sac&kw=jquery%20f
      animation: fadeIn
      year: 2016    
    - title: '"Wordpress Theme Development with Bootstrap"'
      link_name: Brad Hussey, Code College
      link: https://www.udemy.com/bootstrap-to-wordpress/
      animation: fadeIn
      year: 2016
    - title: How to make an Online Store w/ Wordpress - eCommerce Website
      link_name: Hoku Ho
      link: https://www.udemy.com/how-to-make-a-video-blog-website/
      animation: fadeIn
      year: 2016
    - title: Learning KnockoutJS
      link_name: Packt Publishing
      link: https://www.udemy.com/learning-knockoutjs/?src=sac&kw=learning%20knock
      animation: fadeIn
      year: 2016
    - title: The Complete Sass & SCSS Course: From Beginner to Advanced
      link_name: Joe Parys
      link: https://www.udemy.com/sasscourse/
      animation: fadeIn
      year: 2017   
    - title: The Complete JavaScript Course: Build a Real-World Project
      link_name: Jonas Schmedtmann
      link: https://www.udemy.com/the-complete-javascript-course/
      animation: fadeIn
      year: 2017
    - title: Ultimate AngularJS: Build a Real-World App from Scratch
      link_name: Ryan Chenkie
      link: https://www.udemy.com/ultimate-angularjs-course/
      animation: fadeIn   
      year: 2017 
    - title: Bootstrap 4 Quick Start: Code Modern Responsive Websites
      link_name: Brad Hussey, Code College
      link: https://www.udemy.com/bootstrap-4/
      animation: fadeIn
      year: 2017       

            

taxonomy:
    category: right
---
