---
title: EXPERIENCE
date: 13:34 06/21/2012

experience:
  - date: From September 2015 to July 2016
    role: Junior Front End Developer.
    company: ArenaCube
    animation: fadeIn    
  - date: From August 2016 to present
    role: Front End Developer - Sportsbook.
    company: Gaming Innovation Group
    animation: fadeIn


taxonomy:
    category: left
---
