<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => '/Users/DaMike/mike-cv-repo/user/config/site.yaml',
    'modified' => 1466598866,
    'data' => [
        'title' => 'Michael Gauci',
        'description' => 'Front End Developer',
        'author' => [
            'name' => 'Michael Gauci',
            'email' => 'mikegauci@gmail.com'
        ],
        'metadata' => [
            'description' => 'Michael Gaucis resume.'
        ],
        'address' => [
            0 => [
                'line' => 'Flat 8 Cordoba Court'
            ],
            1 => [
                'line' => 'Triq il-Qawra,'
            ],
            2 => [
                'line' => 'San Pawl il-Bahar'
            ]
        ],
        'contact' => [
            0 => [
                'line' => 'mikegauci@gmail.com'
            ],
            1 => [
                'line' => '+356 99256511'
            ]
        ]
    ]
];
