<?php

/* partials/footer.html.twig */
class __TwigTemplate_15b26023e878afb9e993abc4b26cbae147de2536ee80e923185fbb2f19bdf8b2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"row footer\">
   <div class=\"large-12 small-12 medium-12 text-center \">
    <div class=\"border text-center \"></div>
    <p class=\"copyright\">Design by Michael Gauci. Bulit in <a href=\"http://www.getgrav.org\">GRAV.</a></p>
   </div>
</div>";
    }

    public function getTemplateName()
    {
        return "partials/footer.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* <div class="row footer">*/
/*    <div class="large-12 small-12 medium-12 text-center ">*/
/*     <div class="border text-center "></div>*/
/*     <p class="copyright">Design by Michael Gauci. Bulit in <a href="http://www.getgrav.org">GRAV.</a></p>*/
/*    </div>*/
/* </div>*/
